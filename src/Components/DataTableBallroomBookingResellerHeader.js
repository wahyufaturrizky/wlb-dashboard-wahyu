import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TablePagination,
  FormControlLabel,
  Switch,
  Grid,
  Paper,
  InputBase,
  Divider,
  IconButton,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";
import CardBody from "./Card/CardBody.js";
import Button from "./CustomButtons/Button.js";
import CardHeader from "./Card/CardHeader.js";
import SearchIcon from "@material-ui/icons/Search";
import Card from "./Card/Card.js";

// MUI DATA TABLE
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Apartment from "@material-ui/icons/Apartment";
import Visibility from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

// Material UI Icons
import DoneOutline from "@material-ui/icons/DoneOutline";
import avatar from "../assets/img/thumbnail_ballroom.jpg";

function YourCustomRowComponent(props) {
  const {
    tag,
    regDate,
    ballroomName,
    picName,
    phonePic,
    descriptionBallroom,
    pax,
    am,
    status,
    image_ballroom,
    toggleViewBookingReseller,
  } = props;

  let ekstensi = image_ballroom.substring(
    image_ballroom.length,
    image_ballroom.length - 3
  );

  let name_image = image_ballroom.substring(image_ballroom.length - 4, 0);

  return (
    <Box ml={4}>
      <Card>
        <CardBody>
          <Button
            onClick={toggleViewBookingReseller}
            style={{ textAlign: "left" }}
            color='transparent'
          >
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <img
                src={`http://localhost:3000/static/media/${name_image}.c41a0c20.${ekstensi}`}
                alt='thumbnail photo ballroom'
                style={{ width: 300, height: 220 }}
              />
              <Box ml={2}>
                <h2 style={{ fontWeight: "bold" }}>
                  {ballroomName}/{am}/{pax} PAX
                </h2>
                <p>
                  <span style={{ fontWeight: "bold" }}>Reg. Date:</span>{" "}
                  {regDate}
                  <br />
                  <br />
                  <span style={{ fontWeight: "bold" }}>Status:</span>{" "}
                  <Button
                    size='sm'
                    color='whashalal'
                    disabled
                    startIcon={<DoneOutline />}
                  >
                    {status.toUpperCase()}
                  </Button>{" "}
                  <br />
                  <br />
                  <span style={{ fontWeight: "bold" }}>Description:</span>{" "}
                  {descriptionBallroom}
                </p>
              </Box>
            </div>
          </Button>
        </CardBody>
      </Card>
    </Box>
  );
}

const useStyles = makeStyles((theme) => ({
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    padding: "0px 4px",
    display: "flex",
    alignItems: "center",
    width: 300,
    marginRight: theme.spacing(1),
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const DataTableBallroomBookingResellerHeader = ({
  toggleViewBookingReseller,
}) => {
  const classes = useStyles();

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("800px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const columns = [
    "#",
    "Image",
    "Reg. Date",
    "Ballroom Name",
    "PIC Name",
    "Phone PIC",
    "Description Ballroom",
    "Pax",
    "AM",
    "Status",
  ];

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    tableBodyHeight,
    tableBodyMaxHeight,
    customRowRender: (data) => {
      const [
        tag,
        image_ballroom,
        regDate,
        ballroomName,
        picName,
        phonePic,
        descriptionBallroom,
        pax,
        am,
        status,
      ] = data;

      return (
        <tr key={tag}>
          <td colSpan={4} style={{ paddingTop: "10px" }}>
            <YourCustomRowComponent
              toggleViewBookingReseller={toggleViewBookingReseller}
              image_ballroom={image_ballroom}
              tag={tag}
              regDate={regDate}
              ballroomName={ballroomName}
              picName={picName}
              phonePic={phonePic}
              descriptionBallroom={descriptionBallroom}
              pax={pax}
              am={am}
              status={status}
            />
          </td>
        </tr>
      );
    },
  };

  const data = [
    [
      1,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      2,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      3,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      4,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      5,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      6,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      7,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      8,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      9,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      10,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      11,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
    [
      12,
      "thumbnail_ballroom.jpg",
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "082274586011",
      "Ballroom lorem Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rhoncus est sit amet arcu sagittis ultric...",
      "500",
      "AM",
      "available",
    ],
  ];

  return (
    <React.Fragment>
      <Card>
        <CardHeader color='whatsHalal'>
          <Grid container alignItems='center'>
            <Grid item>
              <h4 className={classes.cardTitleWhite}>List Ballroom</h4>
              <p className={classes.cardCategoryWhite}>
                list data List Ballroom
              </p>
            </Grid>
          </Grid>
        </CardHeader>
        <CardBody>
          <FormControl>
            <InputLabel id='demo-simple-select-label'>
              Responsive Option
            </InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={responsive}
              style={{ width: "200px", marginBottom: "10px", marginRight: 10 }}
              onChange={(e) => setResponsive(e.target.value)}
            >
              <MenuItem value={"vertical"}>vertical</MenuItem>
              <MenuItem value={"standard"}>standard</MenuItem>
              <MenuItem value={"simple"}>simple</MenuItem>

              <MenuItem value={"scroll"}>scroll (deprecated)</MenuItem>
              <MenuItem value={"scrollMaxHeight"}>
                scrollMaxHeight (deprecated)
              </MenuItem>
              <MenuItem value={"stacked"}>stacked (deprecated)</MenuItem>
            </Select>
          </FormControl>
          <FormControl>
            <InputLabel id='demo-simple-select-label'>
              Table Body Height
            </InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={tableBodyHeight}
              style={{ width: "200px", marginBottom: "10px", marginRight: 10 }}
              onChange={(e) => setTableBodyHeight(e.target.value)}
            >
              <MenuItem value={""}>[blank]</MenuItem>
              <MenuItem value={"400px"}>400px</MenuItem>
              <MenuItem value={"800px"}>800px</MenuItem>
              <MenuItem value={"100%"}>100%</MenuItem>
            </Select>
          </FormControl>
          <MUIDataTable
            title={"Ballroom list"}
            data={data}
            columns={columns}
            options={options}
          />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  DataTableBallroomBookingResellerHeader
);
