import React, { useState } from "react";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
  Grid,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Alert } from "@material-ui/lab";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import axios from "axios";
import ImageIcon from "@material-ui/icons/Image";
import AssignmentTurnedIn from "@material-ui/icons/AssignmentTurnedIn";
import OpenInNew from "@material-ui/icons/OpenInNew";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

// Icons Material UI
import Person from "@material-ui/icons/Person";
import LocationCity from "@material-ui/icons/LocationCity";
import CardTravel from "@material-ui/icons/CardTravel";
import LocalActivity from "@material-ui/icons/LocalActivity";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBox from "@material-ui/icons/AddBox";
import Add from "@material-ui/icons/Add";
import AddShoppingCart from "@material-ui/icons/AddShoppingCart";
import PeopleOutline from "@material-ui/icons/PeopleOutline";
import DateRange from "@material-ui/icons/DateRange";

// Core Material Component
import Button from "./CustomButtons/Button.js";

// Material UI Icons
import DoneOutline from "@material-ui/icons/DoneOutline";

import InputAdornment from "@material-ui/core/InputAdornment";
import { orange } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  typo: {
    paddingLeft: "25%",
    marginBottom: "10px",
    position: "relative",
    borderBottom: "1px solid #c0c1c2",
    alignItems: "center",
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "17px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px",
  },
  inputMd: {
    width: 400,
  },
  input: {
    display: "none",
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const ViewConfirmSalesOrder = (props) => {
  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
  });

  const [dataProducts, setDataProducts] = useState([]);
  const [openAddImageProductDialog, setOpenAddImageProductDialog] = useState(
    false
  );

  const [openAlertConflict, setOpenAlertConflict] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [openAlertError, setOpenAlertError] = React.useState(false);

  const [progress, setProgress] = React.useState(0);
  const [progressShow, setProgressShow] = React.useState(false);

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    onStartUpload(40);

    const dataAllProduct = new FormData();
    // dataAllProduct.append("product_name", product_name);
    // dataAllProduct.append("brand_name", brand_name);
    // dataAllProduct.append("category", category);
    // dataAllProduct.append("risk_level", risk_level);
    // dataAllProduct.append("status", status);
    // imageProduct.map((key) => dataAllProduct.append("imageProduct", key));

    axios
      .post(`${URL_IRINA}/product`, dataAllProduct, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
      .then((result) => {
        console.log("result = ", result);
        if (result.status === 200) {
          setOpenAlertSuccess(true);
          onStartUpload(100);
        }
      })
      .catch((err) => {
        console.log("error submit halal cert = ", err.response);
        if (err.response.status === 404) {
          setOpenAlertError(true);
        }
        if (err.response.status === 409) {
          setOpenAlertConflict(true);
        }
      });
  };

  React.useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      console.log("setDataProducts = ", result.data);
      setDataProducts(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  const classes = useStyles();

  const onStartUpload = (value) => {
    setProgress(value);
    setProgressShow(true);
    if (value === 100) {
      setTimeout(() => {
        setProgress(value);
        setProgressShow(false);
      }, 2000);
    }
  };

  const onChangeProductFile = (files) => {
    console.log("files", files);
    setFormState({
      ...formState,
      imageProduct: files,
    });
  };

  return (
    <Container>
      <form onSubmit={onSubmit}>
        <div className={classes.typo}>
          <div className={classes.note}>Status Payment</div>
          <Button color='success' disabled startIcon={<DoneOutline />}>
            Complete
          </Button>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Customer Name</div>
          <p>Wahyu Fatur Rizki</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Ballroom Name</div>
          <p>Test Ballroom Name</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>From Date & Time</div>
          <p>29-12-2020 12:30</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>End Date & Time</div>
          <p>30-12-2020 17:30</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Package</div>
          <p>Premium Package</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Options Payment</div>
          <p>Cash</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Package Cost</div>
          <p style={{ textAlign: "right" }}>$ 55000</p>
        </div>
        <div className={classes.typo}>
          <div
            style={{ alignItems: "start", display: "flex" }}
            className={classes.note}
          >
            Add Ons Items
          </div>
          <ul style={{ paddingLeft: 12 }}>
            <li>
              <Grid
                container
                alignItems='center'
                direction='row'
                justify='space-between'
              >
                <Grid item>Convertee</Grid>
                <Grid item>$ 500</Grid>
              </Grid>
            </li>
            <li>
              <Grid
                container
                alignItems='center'
                direction='row'
                justify='space-between'
              >
                <Grid item>Stand Gate</Grid>
                <Grid item>$ 500</Grid>
              </Grid>
            </li>
            <li>
              <Grid
                container
                alignItems='center'
                direction='row'
                justify='space-between'
              >
                <Grid item>Master Ceremony</Grid>
                <Grid item>$ 600</Grid>
              </Grid>
            </li>
            <li>
              <Grid
                container
                alignItems='center'
                direction='row'
                justify='space-between'
              >
                <Grid item>Lighting</Grid>
                <Grid item>$ 670</Grid>
              </Grid>
            </li>
            <li>
              <Grid
                container
                alignItems='center'
                direction='row'
                justify='space-between'
              >
                <Grid item>Sounds Speaker Corner</Grid>
                <Grid item>$ 750</Grid>
              </Grid>
            </li>
          </ul>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Total Add Ons</div>
          <p style={{ textAlign: "right" }}>$ 3750</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Payment by User</div>
          <p style={{ textAlign: "right" }}>$ 58750</p>
        </div>
        <div className={classes.typo}>
          <div className={classes.note}>Total Payment</div>
          <p style={{ textAlign: "right" }}>$ 58750</p>
        </div>
        <Collapse in={openAlertSuccess}>
          <Alert
            severity='success'
            onClose={() => {
              setOpenAlertSuccess(false);
            }}
          >
            Success Created product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertConflict}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertConflict(false);
            }}
          >
            Failed Created product_name, Duplicated product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertError}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertError(false);
            }}
          >
            Failed Created product_name !
          </Alert>
          <br />
        </Collapse>
        {progressShow && <LinearProgressWithLabel value={progress} />}
        <Box display='inline'>
          <Button
            startIcon={<AssignmentTurnedIn />}
            color='whashalal'
            type='submit'
          >
            CONFIRM
          </Button>
        </Box>
        <Box ml={4} display='inline'>
          <Button
            style={{ color: "#8C61FF" }}
            startIcon={<OpenInNew />}
            color='whashalalSecondary'
            onClick={props.toggleExtendPayment}
          >
            EXTEND PAYMENT
          </Button>
        </Box>
      </form>
    </Container>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(ViewConfirmSalesOrder);
