import React, { useState, useEffect } from "react";
import { Box, Chip, TextField, InputLabel } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import axios from "axios";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../../config";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions";

const MultipleValuesSelectProducts = (props) => {
  const [dataProduct, setDataProduct] = useState([]);

  useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}/product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      setDataProduct(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  return (
    <Box>
      <InputLabel shrink>Input multiple list products</InputLabel>
      <Autocomplete
        multiple
        required={true}
        disabled={props.disabled ? props.disabled : null}
        id='products'
        name='products'
        options={dataProduct.map((key) => {
          return {
            product_id: key.product_id,
            product_name: key.product_name,
          };
        })}
        getOptionLabel={(option) => option.product_name}
        // defaultValue={[dataProduct[0].product_name]}
        freeSolo
        onChange={props.onTagsChange}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip
              variant='outlined'
              label={option.product_name || option}
              {...getTagProps({ index })}
            />
          ))
        }
        renderInput={(params) => (
          <TextField
            {...params}
            variant='standard'
            label='Products'
            name='product_field'
            placeholder='Search or Create New Products'
            helperText='Please choose existing products and not create new product'
          />
        )}
      />
    </Box>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  MultipleValuesSelectProducts
);
