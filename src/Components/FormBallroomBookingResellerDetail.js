import React, { useState } from "react";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Alert } from "@material-ui/lab";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import axios from "axios";
import Button from "./CustomButtons/Button.js";
import ImageIcon from "@material-ui/icons/Image";
import Check from "@material-ui/icons/Check";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

// Icons Material UI
import Visibility from "@material-ui/icons/Visibility";
import Book from "@material-ui/icons/Book";
import NavigateNext from "@material-ui/icons/NavigateNext";
import AccountBalance from "@material-ui/icons/AccountBalance";
import Person from "@material-ui/icons/Person";
import EmojiPeople from "@material-ui/icons/EmojiPeople";
import PhoneAndroid from "@material-ui/icons/PhoneAndroid";
import LocationCity from "@material-ui/icons/LocationCity";
import CardTravel from "@material-ui/icons/CardTravel";
import LocalActivity from "@material-ui/icons/LocalActivity";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBox from "@material-ui/icons/AddBox";
import Add from "@material-ui/icons/Add";
import AddShoppingCart from "@material-ui/icons/AddShoppingCart";
import PeopleOutline from "@material-ui/icons/PeopleOutline";
import DateRange from "@material-ui/icons/DateRange";

import InputAdornment from "@material-ui/core/InputAdornment";
import { orange } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  inputMd: {
    width: 400,
  },
  input: {
    display: "none",
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
const top100Films = [
  { title: "The Shawshank Redemption", year: 1994 },
  { title: "The Godfather", year: 1972 },
  { title: "The Godfather: Part II", year: 1974 },
  { title: "The Dark Knight", year: 2008 },
  { title: "12 Angry Men", year: 1957 },
  { title: "Schindler's List", year: 1993 },
  { title: "Pulp Fiction", year: 1994 },
  { title: "The Lord of the Rings: The Return of the King", year: 2003 },
  { title: "The Good, the Bad and the Ugly", year: 1966 },
  { title: "Fight Club", year: 1999 },
  { title: "The Lord of the Rings: The Fellowship of the Ring", year: 2001 },
  { title: "Star Wars: Episode V - The Empire Strikes Back", year: 1980 },
  { title: "Forrest Gump", year: 1994 },
  { title: "Inception", year: 2010 },
  { title: "The Lord of the Rings: The Two Towers", year: 2002 },
  { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
  { title: "Goodfellas", year: 1990 },
  { title: "The Matrix", year: 1999 },
  { title: "Seven Samurai", year: 1954 },
  { title: "Star Wars: Episode IV - A New Hope", year: 1977 },
  { title: "City of God", year: 2002 },
  { title: "Se7en", year: 1995 },
  { title: "The Silence of the Lambs", year: 1991 },
  { title: "It's a Wonderful Life", year: 1946 },
  { title: "Life Is Beautiful", year: 1997 },
  { title: "The Usual Suspects", year: 1995 },
  { title: "Léon: The Professional", year: 1994 },
  { title: "Spirited Away", year: 2001 },
  { title: "Saving Private Ryan", year: 1998 },
  { title: "Once Upon a Time in the West", year: 1968 },
  { title: "American History X", year: 1998 },
  { title: "Interstellar", year: 2014 },
  { title: "Casablanca", year: 1942 },
  { title: "City Lights", year: 1931 },
  { title: "Psycho", year: 1960 },
  { title: "The Green Mile", year: 1999 },
  { title: "The Intouchables", year: 2011 },
  { title: "Modern Times", year: 1936 },
  { title: "Raiders of the Lost Ark", year: 1981 },
  { title: "Rear Window", year: 1954 },
  { title: "The Pianist", year: 2002 },
  { title: "The Departed", year: 2006 },
  { title: "Terminator 2: Judgment Day", year: 1991 },
  { title: "Back to the Future", year: 1985 },
  { title: "Whiplash", year: 2014 },
  { title: "Gladiator", year: 2000 },
  { title: "Memento", year: 2000 },
  { title: "The Prestige", year: 2006 },
  { title: "The Lion King", year: 1994 },
  { title: "Apocalypse Now", year: 1979 },
  { title: "Alien", year: 1979 },
  { title: "Sunset Boulevard", year: 1950 },
  {
    title:
      "Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb",
    year: 1964,
  },
  { title: "The Great Dictator", year: 1940 },
  { title: "Cinema Paradiso", year: 1988 },
  { title: "The Lives of Others", year: 2006 },
  { title: "Grave of the Fireflies", year: 1988 },
  { title: "Paths of Glory", year: 1957 },
  { title: "Django Unchained", year: 2012 },
  { title: "The Shining", year: 1980 },
  { title: "WALL·E", year: 2008 },
  { title: "American Beauty", year: 1999 },
  { title: "The Dark Knight Rises", year: 2012 },
  { title: "Princess Mononoke", year: 1997 },
  { title: "Aliens", year: 1986 },
  { title: "Oldboy", year: 2003 },
  { title: "Once Upon a Time in America", year: 1984 },
  { title: "Witness for the Prosecution", year: 1957 },
  { title: "Das Boot", year: 1981 },
  { title: "Citizen Kane", year: 1941 },
  { title: "North by Northwest", year: 1959 },
  { title: "Vertigo", year: 1958 },
  { title: "Star Wars: Episode VI - Return of the Jedi", year: 1983 },
  { title: "Reservoir Dogs", year: 1992 },
  { title: "Braveheart", year: 1995 },
  { title: "M", year: 1931 },
  { title: "Requiem for a Dream", year: 2000 },
  { title: "Amélie", year: 2001 },
  { title: "A Clockwork Orange", year: 1971 },
  { title: "Like Stars on Earth", year: 2007 },
  { title: "Taxi Driver", year: 1976 },
  { title: "Lawrence of Arabia", year: 1962 },
  { title: "Double Indemnity", year: 1944 },
  { title: "Eternal Sunshine of the Spotless Mind", year: 2004 },
  { title: "Amadeus", year: 1984 },
  { title: "To Kill a Mockingbird", year: 1962 },
  { title: "Toy Story 3", year: 2010 },
  { title: "Logan", year: 2017 },
  { title: "Full Metal Jacket", year: 1987 },
  { title: "Dangal", year: 2016 },
  { title: "The Sting", year: 1973 },
  { title: "2001: A Space Odyssey", year: 1968 },
  { title: "Singin' in the Rain", year: 1952 },
  { title: "Toy Story", year: 1995 },
  { title: "Bicycle Thieves", year: 1948 },
  { title: "The Kid", year: 1921 },
  { title: "Inglourious Basterds", year: 2009 },
  { title: "Snatch", year: 2000 },
  { title: "3 Idiots", year: 2009 },
  { title: "Monty Python and the Holy Grail", year: 1975 },
];

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const FormBallroomBookingResellerDetail = (props) => {
  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
  });

  const [dataProducts, setDataProducts] = useState([]);
  const [openAddImageProductDialog, setOpenAddImageProductDialog] = useState(
    false
  );

  const [openAlertConflict, setOpenAlertConflict] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [openAlertError, setOpenAlertError] = React.useState(false);

  const [progress, setProgress] = React.useState(0);
  const [progressShow, setProgressShow] = React.useState(false);

  const [values, setValues] = useState({ val: [] });
  const [nameVariation, setNameVariation] = useState("");

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    onStartUpload(40);

    const dataAllProduct = new FormData();
    // dataAllProduct.append("product_name", product_name);
    // dataAllProduct.append("brand_name", brand_name);
    // dataAllProduct.append("category", category);
    // dataAllProduct.append("risk_level", risk_level);
    // dataAllProduct.append("status", status);
    // imageProduct.map((key) => dataAllProduct.append("imageProduct", key));

    axios
      .post(`${URL_IRINA}/product`, dataAllProduct, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
      .then((result) => {
        console.log("result = ", result);
        if (result.status === 200) {
          setOpenAlertSuccess(true);
          onStartUpload(100);
        }
      })
      .catch((err) => {
        console.log("error submit halal cert = ", err.response);
        if (err.response.status === 404) {
          setOpenAlertError(true);
        }
        if (err.response.status === 409) {
          setOpenAlertConflict(true);
        }
      });
  };

  React.useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      console.log("setDataProducts = ", result.data);
      setDataProducts(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  const classes = useStyles();

  const onStartUpload = (value) => {
    setProgress(value);
    setProgressShow(true);
    if (value === 100) {
      setTimeout(() => {
        setProgress(value);
        setProgressShow(false);
      }, 2000);
    }
  };

  const onChangeProductFile = (files) => {
    console.log("files", files);
    setFormState({
      ...formState,
      imageProduct: files,
    });
  };

  return (
    <Container>
      <form onSubmit={onSubmit}>
        <ThemeProvider theme={theme}>
          <h3>Check Availability</h3>
          <p>
            *Please fill in the available check column first, to check whether
            the ballroom can be booked
          </p>
          <TextField
            variant='outlined'
            type='datetime-local'
            name='from_date'
            label='From Date'
            id='from_date'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill From Date'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <DateRange />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='datetime-local'
            name='end_date'
            label='End Date'
            id='end_date'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill End Date'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <DateRange />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='datetime-local'
            name='subject'
            label='Subject'
            id='subject'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill Subject'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <Book />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <Button startIcon={<Visibility />} color='whashalal' type='submit'>
            CHECK AVAILABLE
          </Button>
          <br />
          <br />
          <h4>Result Check Status:</h4>
          <p>
            Your choice of 01 Dec 2020 01:00 PM until 01 Dec 2020 05:00 PM is
          </p>
          <Button disabled color='success' startIcon={<Check />}>
            AVAILABLE
          </Button>
          <h3>Booking if available</h3>
          <TextField
            variant='outlined'
            type='text'
            name='fucntion_type'
            label='Fucntion Type'
            id='fucntion_type'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill Fucntion Type'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <AccountBalance />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='number'
            name='attendees'
            label='Attendees'
            id='attendees'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill Attendees'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <EmojiPeople />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='datetime-local'
            name='from_date'
            label='From Date'
            id='from_date'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill From Date'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <DateRange />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='datetime-local'
            name='end_date'
            label='End Date'
            id='end_date'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill End Date'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <DateRange />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            variant='outlined'
            type='datetime-local'
            name='subject'
            label='Subject'
            id='subject'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill Subject'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <Book />
                </InputAdornment>
              ),
            }}
          />
          <br />
          <br />
          <TextField
            multiline
            rows={4}
            variant='outlined'
            type='datetime-local'
            name='subject'
            label='Subject'
            id='subject'
            margin='normal'
            helperText='Please specify the name'
            required={true}
            className={classes.inputMd}
            placeholder='Fill Subject'
            onChange={(e) => handleTextFieldChange(e)}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <Book />
                </InputAdornment>
              ),
            }}
          />
        </ThemeProvider>
        <Collapse in={openAlertSuccess}>
          <Alert
            severity='success'
            onClose={() => {
              setOpenAlertSuccess(false);
            }}
          >
            Success Created product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertConflict}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertConflict(false);
            }}
          >
            Failed Created product_name, Duplicated product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertError}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertError(false);
            }}
          >
            Failed Created product_name !
          </Alert>
          <br />
        </Collapse>
        {progressShow && <LinearProgressWithLabel value={progress} />}
        <Button
          startIcon={<NavigateNext />}
          color='whashalal'
          // type='submit'
          onClick={props.togglePaymentBookingResellerDetail}
        >
          NEXT
        </Button>
      </form>
    </Container>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  FormBallroomBookingResellerDetail
);
