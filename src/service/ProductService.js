import axios from "axios";

export default class ProductService {
  getProductsSmall() {
    return axios.get("data/products-small.json").then((res) => res.data.data);
  }

  getProducts() {
    return axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        console.log("getProducts", res);
        return res;
      });
  }

  getProductsDetail(e) {
    return axios.get(e).then((res) => {
      console.log("getProductsDetail", res);
      return res;
    });
  }

  getProductsWithOrdersSmall() {
    return axios
      .get("data/products-orders-small.json")
      .then((res) => res.data.data);
  }
}
