module.exports = {
  module: {
    rules: [
      {
        include: path.resolve(
          __dirname,
          "./node_modules/react-image-gallery/styles/scss/image-gallery.scss"
        ),
        test: /\.s?css/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
};
