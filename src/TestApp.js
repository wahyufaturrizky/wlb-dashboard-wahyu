import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import Form from "./Components/Form";
import UpdateForm from "./Components/Update";
import DataTable from "./Components/Table";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { URL_ANASTASIA } from "./config";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  const [modal, setModal] = useState(false);

  const [formState, setFormState] = useState({
    email: "",
    organization_name: "",
    category: "",
    street_name: "",
    organization_contact: "",
    building_name: "",
    postal_code: "",
    city: "",
    country: "",
    country_iso_code: "",
  });

  const handleTextFieldChange = (e) =>
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .put(`${URL_ANASTASIA}/organization`, {
        ...formState,
      })
      .then((result) => {
        if (result.status === 200) {
          setModal(false);
          console.log("success", result);
        }
      })
      .catch((err) => console.log("error", err));
  };

  const toggle = (e) => {
    setModal(!modal);
    setFormState(e);
  };

  return (
    <div>
      <Form />
      <DataTable buttonCallBack={toggle} />
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <UpdateForm
          handleTextFieldChange={handleTextFieldChange}
          onSubmit={onSubmit}
          formState={formState}
        />
        <ModalFooter>
          <Button color='secondary' onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default App;
