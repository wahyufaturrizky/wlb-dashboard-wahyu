import React from "react";
import PropTypes from "prop-types";
// @material-ui/core Components
import { makeStyles } from "@material-ui/core/styles";
// core Components
import styles from "../../assets/jss/material-dashboard-react/Components/typographyStyle.js";

const useStyles = makeStyles(styles);

export default function Primary(props) {
  const classes = useStyles();
  const { children } = props;
  return (
    <div className={classes.defaultFontStyle + " " + classes.primaryText}>
      {children}
    </div>
  );
}

Primary.propTypes = {
  children: PropTypes.node,
};
