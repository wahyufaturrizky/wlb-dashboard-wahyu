import React, { useState } from "react";
import { Box, TextField, Grid, InputLabel } from "@material-ui/core";
import Button from "../CustomButtons/Button.js";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  inputMd: {
    width: 500,
  },
}));

export default function TextFieldIncreamentDecrementSignature({
  inputMd,
  disabled,
  captureSignatureList,
}) {
  const [quantityField, setQuantityField] = React.useState(1);
  const [quantityFieldSignature, setquantityFieldSignature] = React.useState([
    0,
  ]);

  const [formStateSignature, setFormStateSignature] = useState({
    full_name_value: "",
    title_value: "",
    additional_information_value: "",
  });

  const incrementFieldSignatures = () => {
    const {
      full_name_value,
      title_value,
      additional_information_value,
    } = formStateSignature;

    setQuantityField(parseInt(quantityField + 1));
    setquantityFieldSignature([...quantityFieldSignature, quantityField]);

    captureSignatureList(
      full_name_value,
      title_value,
      additional_information_value
    );
  };

  const decrementFieldSignatures = () => {
    let removequantityFieldSignature = quantityFieldSignature;
    console.log(
      "before quantityFieldSignature = ",
      removequantityFieldSignature
    );
    removequantityFieldSignature.pop();
    setquantityFieldSignature(removequantityFieldSignature);

    console.log(
      "after quantityFieldSignature = ",
      removequantityFieldSignature
    );
  };

  const handleTextFieldChangeSignature = (e) => {
    setFormStateSignature({
      ...formStateSignature,
      [e.target.name]: e.target.value,
    });
  };

  const classes = useStyles();

  return (
    <>
      {quantityFieldSignature.map((key, index) => (
        <Box key={index} mt={3}>
          <InputLabel shrink>Signature {index + 1}</InputLabel>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <TextField
                disabled={disabled ? disabled : null}
                type='text'
                label='Full Name'
                id='full_name_value'
                name='full_name_value'
                onChange={(e) => handleTextFieldChangeSignature(e)}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                disabled={disabled ? disabled : null}
                type='text'
                label='Title'
                id='title_value'
                name='title_value'
                onChange={(e) => handleTextFieldChangeSignature(e)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                type='text'
                disabled={disabled ? disabled : null}
                multiline
                label='Additional Information'
                id='additional_information_value'
                name='additional_information_value'
                className={classes.inputMd}
                onChange={(e) => handleTextFieldChangeSignature(e)}
              />
            </Grid>
          </Grid>
        </Box>
      ))}

      <Box mt={2}>
        <Grid
          container
          display='row'
          justify='space-between'
          alignItems='center'
        >
          <Grid item>
            <Button
              disabled={disabled ? disabled : null}
              onClick={incrementFieldSignatures}
              color='whashalal'
            >
              Add Signature
            </Button>
          </Grid>
          <Grid item>
            <Button
              disabled={disabled ? disabled : null}
              onClick={decrementFieldSignatures}
              color='whashalal'
            >
              Remove Signature
            </Button>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
