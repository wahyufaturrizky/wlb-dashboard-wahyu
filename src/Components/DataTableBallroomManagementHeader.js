import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TablePagination,
  FormControlLabel,
  Switch,
  Grid,
  Paper,
  InputBase,
  Divider,
  IconButton,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";
import CardBody from "./Card/CardBody.js";
import Button from "./CustomButtons/Button.js";
import CardHeader from "./Card/CardHeader.js";
import SearchIcon from "@material-ui/icons/Search";
import Card from "./Card/Card.js";

// MUI DATA TABLE
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Apartment from "@material-ui/icons/Apartment";
import Visibility from "@material-ui/icons/Visibility";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

const useStyles = makeStyles((theme) => ({
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
  root: {
    padding: "0px 4px",
    display: "flex",
    alignItems: "center",
    width: 300,
    marginRight: theme.spacing(1),
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const DataTableBallroomManagementHeader = ({
  toggleRegisterBallroomManagement,
  toggleUpdateBallroomManagement,
  toggleViewBallroomManagement,
}) => {
  const classes = useStyles();

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const columns = [
    "#",
    "Reg. Date",
    "Ballroom Name",
    "PIC Name",
    "Update Date",
    "Phone PIC",
    "Status",
    {
      name: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <Button
              onClick={toggleViewBallroomManagement}
              size='sm'
              color='success'
              startIcon={<Visibility />}
            >
              View
            </Button>
          );
        },
      },
    },
    {
      name: "",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <Button
              onClick={toggleUpdateBallroomManagement}
              size='sm'
              color='info'
              startIcon={<EditIcon />}
            >
              Edit
            </Button>
          );
        },
      },
    },
  ];

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    tableBodyHeight,
    tableBodyMaxHeight,
  };

  const data = [
    [
      1,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      2,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      3,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      4,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      5,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      6,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      7,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      8,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      9,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      10,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      11,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
    [
      12,
      "01-12-2020 12:30",
      "Ballroom name test",
      "Wahyu Fatur Rizki",
      "01-12-2020 12:30",
      "082274586011",
      "Active",
    ],
  ];

  return (
    <React.Fragment>
      <Card>
        <CardHeader color='whatsHalal'>
          <Grid
            container
            direction='row'
            justify='space-between'
            alignItems='center'
          >
            <Grid item>
              <h4 className={classes.cardTitleWhite}>List Ballroom</h4>
              <p className={classes.cardCategoryWhite}>
                list data List Ballroom
              </p>
            </Grid>
            <Grid
              direction='row'
              justify='space-between'
              alignItems='center'
              item
            >
              <Grid item>
                <Button
                  color='whashalal'
                  onClick={toggleRegisterBallroomManagement}
                  startIcon={<Apartment />}
                >
                  Create Ballroom
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </CardHeader>
        <CardBody>
          <FormControl>
            <InputLabel id='demo-simple-select-label'>
              Responsive Option
            </InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={responsive}
              style={{ width: "200px", marginBottom: "10px", marginRight: 10 }}
              onChange={(e) => setResponsive(e.target.value)}
            >
              <MenuItem value={"vertical"}>vertical</MenuItem>
              <MenuItem value={"standard"}>standard</MenuItem>
              <MenuItem value={"simple"}>simple</MenuItem>

              <MenuItem value={"scroll"}>scroll (deprecated)</MenuItem>
              <MenuItem value={"scrollMaxHeight"}>
                scrollMaxHeight (deprecated)
              </MenuItem>
              <MenuItem value={"stacked"}>stacked (deprecated)</MenuItem>
            </Select>
          </FormControl>
          <FormControl>
            <InputLabel id='demo-simple-select-label'>
              Table Body Height
            </InputLabel>
            <Select
              labelId='demo-simple-select-label'
              id='demo-simple-select'
              value={tableBodyHeight}
              style={{ width: "200px", marginBottom: "10px", marginRight: 10 }}
              onChange={(e) => setTableBodyHeight(e.target.value)}
            >
              <MenuItem value={""}>[blank]</MenuItem>
              <MenuItem value={"400px"}>400px</MenuItem>
              <MenuItem value={"800px"}>800px</MenuItem>
              <MenuItem value={"100%"}>100%</MenuItem>
            </Select>
          </FormControl>
          <MUIDataTable
            title={"Ballroom list"}
            data={data}
            columns={columns}
            options={options}
          />
        </CardBody>
      </Card>
    </React.Fragment>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  DataTableBallroomManagementHeader
);
