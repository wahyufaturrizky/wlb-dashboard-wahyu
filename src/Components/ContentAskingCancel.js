import React, { useState } from "react";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Alert } from "@material-ui/lab";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import axios from "axios";
import Button from "./CustomButtons/Button.js";
import ImageIcon from "@material-ui/icons/Image";
import CheckCircle from "@material-ui/icons/CheckCircle";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

// Icons Material UI
import Update from "@material-ui/icons/Update";
import DateRange from "@material-ui/icons/DateRange";

// Components
import Muted from "./Typography/Muted.js";

import InputAdornment from "@material-ui/core/InputAdornment";
import { orange } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  inputMd: {
    width: 400,
  },
  input: {
    display: "none",
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const ContentAskingCancel = (props) => {
  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
    type_extend: "",
  });

  const [dataProducts, setDataProducts] = useState([]);
  const [openAddImageProductDialog, setOpenAddImageProductDialog] = useState(
    false
  );

  const [openAlertConflict, setOpenAlertConflict] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [openAlertError, setOpenAlertError] = React.useState(false);

  const [progress, setProgress] = React.useState(0);
  const [progressShow, setProgressShow] = React.useState(false);

  const handleTextFieldChange = (e) => {
    console.log("e", e);
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    onStartUpload(40);

    const dataAllProduct = new FormData();
    // dataAllProduct.append("product_name", product_name);
    // dataAllProduct.append("brand_name", brand_name);
    // dataAllProduct.append("category", category);
    // dataAllProduct.append("risk_level", risk_level);
    // dataAllProduct.append("status", status);
    // imageProduct.map((key) => dataAllProduct.append("imageProduct", key));

    axios
      .post(`${URL_IRINA}/product`, dataAllProduct, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
      .then((result) => {
        console.log("result = ", result);
        if (result.status === 200) {
          setOpenAlertSuccess(true);
          onStartUpload(100);
        }
      })
      .catch((err) => {
        console.log("error submit halal cert = ", err.response);
        if (err.response.status === 404) {
          setOpenAlertError(true);
        }
        if (err.response.status === 409) {
          setOpenAlertConflict(true);
        }
      });
  };

  React.useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      console.log("setDataProducts = ", result.data);
      setDataProducts(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  const classes = useStyles();

  const onStartUpload = (value) => {
    setProgress(value);
    setProgressShow(true);
    if (value === 100) {
      setTimeout(() => {
        setProgress(value);
        setProgressShow(false);
      }, 2000);
    }
  };

  const onChangeProductFile = (files) => {
    console.log("files", files);
    setFormState({
      ...formState,
      imageProduct: files,
    });
  };

  const { type_extend } = formState;
  console.log("type_extend", type_extend);
  return (
    <Container>
      <form onSubmit={onSubmit}>
        <div>
          <h3>
            Any cancellation of an order, will be subject to additional fees
          </h3>
          <Muted>You will be charged $ 2000 for canceling this order!</Muted>
        </div>
        <Collapse in={openAlertSuccess}>
          <Alert
            severity='success'
            onClose={() => {
              setOpenAlertSuccess(false);
            }}
          >
            Success Created product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertConflict}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertConflict(false);
            }}
          >
            Failed Created product_name, Duplicated product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertError}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertError(false);
            }}
          >
            Failed Created product_name !
          </Alert>
          <br />
        </Collapse>
        {progressShow && <LinearProgressWithLabel value={progress} />}
        <Box mt={4}>
          <Button startIcon={<CheckCircle />} color='whashalal' type='submit'>
            Yes, I’m agree
          </Button>
        </Box>
      </form>
    </Container>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(ContentAskingCancel);
